filetype plugin on

call plug#begin('~/.vim/plugged')
    " Status bar plugins
    " Plug 'hoob3rt/lualine.nvim'
    " Plug 'kyazdani42/nvim-web-devicons'
    Plug 'vim-airline/vim-airline'
    Plug 'ryanoasis/vim-devicons'

    " Themes
    Plug 'gruvbox-community/gruvbox'
    Plug 'sainnhe/gruvbox-material'
    Plug 'folke/tokyonight.nvim'
    Plug 'jiangmiao/auto-pairs'


    " Editing plugins
	Plug 'sbdchd/neoformat'
call plug#end()


" ==== SETS ====
let mapleader = " "             " Leader key remap
set relativenumber              " Set relative numbers
set number                      " The line where the cursor is, is the actual line number

set clipboard=unnamed           " Sharing clipboard with OS

set noerrorbells                " No annoying bling bling when pressing backspace
set nobackup                    " No backup files
set nowritebackup               " Only in case you don't want a backup file while editing
set noswapfile                  " No .swp or anything else
set incsearch                   " Highlight the next matching word instead of all matches

" Indentation parameters
set tabstop=4                   " Number of spaces the <Tab> 'count' for
set softtabstop=4               " Number of spaces the <Tab> 'count' but in the insersion
set shiftwidth=4                " Actually the same as tabstop
set expandtab                   " In insert mode, <Tab> inserts spaces instead of a tabulation
set smartindent	                " Try to indent when it can according to the file type

" Undo redo parameters
set undodir=~/.vim/undodir      " Set the undodir folder
set undofile                    " A kind of history of undos for a given file

" GUI parameters
set lines=60                    " Default line size of the GUI window
set columns=180                 " Default column size of the GUI window
set guifont=Consolas:h10        " Font of the GUI window


" Fix the not working backspce in Windows
set backspace=2
set backspace=indent,eol,start


" ==== AUTO CMDS ====
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

syntax on

let g:jpilleux_colorscheme = "gruvbox"

fun! SetColorScheme()
    let g:gruvbox_contrast_dark = 'hard'
    if exists('+termguicolors')
        let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    endif
    let g:gruvbox_invert_selection='0'
    let g:gruvbox_transparent_bg = 1

    set background=dark
	colorscheme gruvbox

    highlight ColorColumn ctermbg=0 guibg=grey
endfun
call SetColorScheme()

augroup J_PILLEUX
    autocmd!
    autocmd BufWritePre * :call TrimWhitespace()
    autocmd BufEnter,BufWinEnter,TabEnter *.rs :lua require'lsp_extensions.inlay_hints{}
augroup END


" ==== REMAPS ====
" Simplier move through panes
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" Move lines up and down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Copy pasting remaps
vnoremap <leader>p "_dP

nnoremap <leader>y "+y
vnoremap <leader>y "+y
nnoremap <leader>Y gg"+yG

nnoremap <leader>d "_d
vnoremap <leader>d "_d
